/*
 * Opdracht :
 * 
 *     Beheer-scherm                                                    Projector-scherm
 * +-----------------------+--------------------------------------+ +-------------------------------------------------------------------------+
 * |                       |                                      | |                                                                         |
 * |    input              |                                      | |    Data die ingegeven is in beheer-scherm                               |
 * |    input              |                                      | |                                                                         |
 * |    input              |                                      | |                                                                         |
 * |    input              |       preview/voorbeeld              | |                                                                         |
 * |                       |       Projector-scherm               | |                                                                         |
 * |                       |                                      | |                                                                         |
 * |                       |                                      | |                                                                         |
 * |                       |                                      | |                                                                         |
 * |     update            |                                      | |                                                                         |
 * |                       |                                      | |                                                                         |
 * |                       |                                      | |                                                                         |
 * +-----------------------+--------------------------------------+ |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *  Update van schermen via Observer                                |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  |                                                                         |
 *                                                                  +-------------------------------------------------------------------------+
 * 
 * 
 * 
 */
package be.janwagemakers.projector;

import be.janwagemakers.projector.model.Model;
import be.janwagemakers.projector.view.Gui;

public class Application {
	private Model model;	
	private Gui gui;

	public Application() {
		model = new Model();	// model --> hier zit de data
		gui = new Gui(model);	// GUI 
	}

	public static void main(String[] args) {
		new Application();
	}

}
