/* 
 * Klasse voor het tonen van een beheer-scherm.
 * Een beheer-scherm bestaat uit twee delen:
 * 	- linkerzijde : ingeven van vragen en antwoorden
 *  - rechterzijde : preview van projector-scherm
 */

package be.janwagemakers.projector.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import be.janwagemakers.projector.controler.UpdateAntwoorden;
import be.janwagemakers.projector.controler.UpdateVragen;
import be.janwagemakers.projector.model.Model;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class Beheer extends JFrame {
	
	// Constanten
	static final int startX = 0;						// Waar venster op scherm plaatsen X
	static final int startY = 100;						// Waar venster op scherm plaatsen Y
	static final int beheerX = 260;						// Afmeting beheer gedeelte X
	static final int voorbeeldX = 640;					// Afmeting voorbeeld geddelte X
	static final int sizeX = beheerX + voorbeeldX;		// Afmeting venster X
	static final int sizeY = 480;						// Afmeting venster Y
	
	private JPanel contentPane;
	private JFrame frame;
	private JTextField txtWelkeKleurHeeft;
	private JTextField txtBlauw;
	private UpdateAntwoorden actionListenerA;
	private UpdateVragen actionListenerV;
	private Vraag vraag1;
	private Vraag vraag2;
	private Vraag vraag3;
	private Vraag vraag4;
	private Model model;
	
	/**
	 * Create the frame.
	 */
	public Beheer(Model _model) {
		this.model = _model;
		frame = new JFrame();
		frame.setTitle("Beheer-scherm");
		frame.setResizable(false);
		frame.setBounds(startX, startY, sizeX, sizeY);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		// Tonen van de velden om vragen en antwoorden in te geven
		vraag1 = new Vraag(frame, 10, 10);
		vraag1.setTitel("Vraag 1 :");
		vraag1.setVraag("Welke kleur heeft een smurf?");
		vraag1.setAntwoord("Blauw");
		vraag2 = new Vraag(frame, 10, 100);
		vraag2.setTitel("Vraag 2 :");
		vraag2.setVraag("Geef de wet van Ohm");
		vraag2.setAntwoord("U = I x R");
		vraag3 = new Vraag(frame, 10, 190);
		vraag3.setTitel("Vraag 3 :");
		vraag3.setVraag("Wat is de eenheid van elektrisch vermogen?");
		vraag3.setAntwoord("Watt (W)");
		vraag4 = new Vraag(frame, 10, 280);
		vraag4.setTitel("Vraag 4 :");
		vraag4.setVraag("Kleur van een geŽlektrocuteerde smurf?");
		vraag4.setAntwoord("Zwart");

		// Tonen van knoppen
		JButton btnA = new JButton("Update Antwoorden");
		btnA.setBounds(27, 400, 200, 23);
		actionListenerA = new UpdateAntwoorden(_model, this);
		btnA.addActionListener(actionListenerA);
		frame.getContentPane().add(btnA);

		JButton btnV = new JButton("Update Vragen");
		btnV.setBounds(27, 370, 200, 23);
		actionListenerV = new UpdateVragen(_model, this);
		btnV.addActionListener(actionListenerV);
		frame.getContentPane().add(btnV);
		
		// Scheiden van input en preview d.m.v. rode lijn
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setForeground(Color.RED);
		separator.setBounds(beheerX, 0, 1, sizeY);
		frame.getContentPane().add(separator);
		
		// Titel in voorbeeldscherm
		JLabel lblTitel = new JLabel("Voorbeeld-scherm");
		lblTitel.setBounds(beheerX + 200, 10, 400,40);
		lblTitel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblTitel.setForeground(Color.RED);
		frame.getContentPane().add(lblTitel);
		
		// Tonen van projector-voorbeeld scher
		ProjectorData data = new ProjectorData(frame, beheerX + 10 , 60, 1);
		model.addObserver(data);
								
		frame.setVisible(true);
	}

	// Inhoud van de antwoorden teruggeven als een String-array
	public String[] getAntwoorden() {
		String[] antwoorden = new String[4];
		antwoorden[0] = vraag1.getAntwoord();
		antwoorden[1] = vraag2.getAntwoord();
		antwoorden[2] = vraag3.getAntwoord();
		antwoorden[3] = vraag4.getAntwoord();
		return antwoorden;
	}

	// Inhoud van de vragen teruggeven als een String-array
	public String[] getVragen() {
		String[] vragen = new String[4];
		vragen[0] = vraag1.getVraag();
		vragen[1] = vraag2.getVraag();
		vragen[2] = vraag3.getVraag();
		vragen[3] = vraag4.getVraag();
		return vragen;
	}
	
    // Geef X afmeting van voorbeeld venster	
	public static int getSizeX() {
		return voorbeeldX;
	}
	
	// Geef Y afmeting van voorbeeld venster
	public static int getSizeY() {
		return sizeY;
	}

}
