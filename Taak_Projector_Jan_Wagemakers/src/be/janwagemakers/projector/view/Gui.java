/*
 * Aanmaken van de GUI
 * 
 * GUI bestaat uit;
 *   - beheer-scherm
 *   	- linkerzijde waar de vragen/antwoorden worden ingegeven
 *   	- preview-scherm met dezelfde inhoud als op het projector-scherm
 *   - projector-scherm
 *   
 * Data voor de GUI worden bijgehouden in Model model
 *   
 */
package be.janwagemakers.projector.view;

import be.janwagemakers.projector.model.Model;

public class Gui {
	private Beheer beheer;
	private Projector projector;
	private Model model;

	public Gui(Model _model) {
		// set model
		this.model = _model;
		
		// create views
		beheer = new Beheer(model);
		projector = new Projector(model);
		
		// Init model with values
		model.setVragen(beheer.getVragen()); // toon de standaardvragen
		model.emptyAntwoorden();             // zonder de antwoorden. De standaard antwoorden verschijnen als op de knop "update antwoorden" is geklikt
	}
}
