/* 
 * Klasse voor het tonen van de DATA.
 * 
 * Instanties van deze klasse worden zowel gebruikt in het beheer-scherm (voorbeeld)
 * als in het projector-scherm. 
 * 
 * Deze klasse zorgt er ook voor dat DATA op de schermen via observer
 * automatisch geupdated wordt.
 *  
 */
package be.janwagemakers.projector.view;

import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import be.janwagemakers.projector.model.Model;

public class ProjectorData implements Observer {
	private Regel regel1;  	// Vraag 1 : .............
							// Antwoord 1 : ..............
	private Regel regel2;	// Vraag 2 : .............
							// Antwoord 2 : ..............
	private Regel regel3;	// Vraag 3 : .............
							// Antwoord 3 : ..............
	private Regel regel4;	// Vraag 4 : .............
							// Antwoord 4 : ..............

	public ProjectorData(JFrame frame, int x, int y, float size) {
		// x, y = positie
		// size vergrotings-factor
		
		// Eerste Vraag/Antwoord-''regel''
		regel1 = new Regel(frame, x, y, size);
		y=y+100;
		
		// Tweede Vraag/Antwoord-''regel''
		regel2 = new Regel(frame, x, y, size);
		y=y+100;
		
		// Derde Vraag/Antwoord-''regel''3
		regel3 = new Regel(frame, x, y, size);
		y=y+100;
		
		// Vierde Vraag/Antwoord-''regel''
		regel4 = new Regel(frame, x, y, size);
	}

	@Override
	public void update(Observable o, Object arg) {
		// Updaten van de weergave op het scherm met de gegevens die
		// zich in Model model bevinden.
		String[] antwoorden = new String[4];
		String[] vragen = new String[4];
		// System.out.println("Update methode aangeroepen!");
		Model model = (Model)o;
		antwoorden = model.getAntwoorden();
		regel1.setAntwoord(antwoorden[0]);
		regel2.setAntwoord(antwoorden[1]);
		regel3.setAntwoord(antwoorden[2]);
		regel4.setAntwoord(antwoorden[3]);
		vragen = model.getVragen();
		regel1.setVraag(vragen[0]);
		regel2.setVraag(vragen[1]);
		regel3.setVraag(vragen[2]);
		regel4.setVraag(vragen[3]);
	}
}
