/* 
 * Klasse voor het tonen van een projector-scherm.
 * 
 */

package be.janwagemakers.projector.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import be.janwagemakers.projector.model.Model;

public class Projector extends JFrame {
	
	// Constanten 
	static final int startX = 900;							// Waar venster op scherm plaatsen X
	static final int startY = 100;							// Waar venster op scherm plaatsen Y
	static final float size = (float) 1.6; 					// Hoeveel groter t.o.v. het voorbeeldvenster moet het projectorvenster zijn?
	static final int sizeX = (int) (640*size);				// Afmeting venster X
	static final int sizeY = (int) (480*size);				// Afmeting venster Y
	
	JPanel contentPane;
	private JFrame frame;
	private Model model;

	/**
	 * Create the frame.
	 */
	public Projector(Model _model) {
		this.model = _model;
		
		// JFrame maken
		String titel = "Projector-scherm";
		frame = new JFrame();
		frame.setTitle(titel);
		frame.setResizable(false);
		frame.setBounds(startX, startY, sizeX, sizeY);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTitel = new JLabel(titel);
		lblTitel.setBounds((int) (200*size), (int) (10*size), (int) (400*size),(int) (40*size));
		lblTitel.setFont(new Font("Tahoma", Font.PLAIN, (int) (24*size)));
		lblTitel.setForeground(Color.RED);
		frame.getContentPane().add(lblTitel);
		
		// De data zelf op het scherm zetten (aparte klasse zo dat deze ook in beheer-preview gebruikt kan worden).
		ProjectorData data = new ProjectorData(frame, 10, 60, size); 
		
		// data(view) --> Observer
		model.addObserver(data);
		
		frame.setVisible(true);
	}
}