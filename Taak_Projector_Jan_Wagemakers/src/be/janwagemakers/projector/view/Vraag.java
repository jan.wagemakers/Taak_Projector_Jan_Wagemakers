/*
 * Maak op het beheer-scherm de combinatie JLabel + 2 * JTextField aan.
 * 
 * Vraag x 	(JLabel)     = titel, bv. vraag 1
 * Vraag 	(JTextField) = input vraag
 * Antwoord (JTextField) = input antwoord
 * 
 */
package be.janwagemakers.projector.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Vraag {
	private JLabel titel;
	private JTextField vraag;
	private JTextField antwoord;

	public Vraag(JFrame frame, int x, int y) {
		// Plaatsing op scherm volgens x, y
		
		// Maak Label (vraag x)
		titel = new JLabel();
		titel.setBounds(x, y, 150, 23);
		frame.getContentPane().add(titel);
		
		// Knop om velden leeg te maken
		JButton btnWis = new JButton("Maak Leeg");
		btnWis.setBounds(x + 100, y, 138, 23);
		frame.getContentPane().add(btnWis);
		
		// Schuif op naar beneden
		y=y+25;
		
		// Maak input voor vraag
		vraag = new JTextField();
		vraag.setBounds(x, y, 238, 20);
		frame.getContentPane().add(vraag);
		vraag.setColumns(10);
			
		// Schuif op naar beneden
		y=y+25;
		
		// Maak input voor antwoord
		antwoord = new JTextField();
		antwoord.setColumns(10);
		antwoord.setBounds(x, y, 238, 20);
		antwoord.setForeground(Color.blue);
		frame.getContentPane().add(antwoord);
	
		// Op de knop maak leeg drukken --> velden leegmaken
		btnWis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vraag.setText("");
				antwoord.setText("");
				vraag.grabFocus();		// En cursor in eerste vraag veld zetten.
			}
		});
	}
	
	// Instellen String label
	public void setTitel(String titel) {
		this.titel.setText(titel);
	}
	
	// Instellen String input vraag
	public void setVraag(String vraag) {
		this.vraag.setText(vraag);
	}

	// Instellen String input antwoord
	public void setAntwoord(String antwoord) {
		this.antwoord.setText(antwoord);
	}
	
	public String getVraag() {
		return this.vraag.getText();
	}
	
	public String getAntwoord() {
		return this.antwoord.getText();
	}
}
