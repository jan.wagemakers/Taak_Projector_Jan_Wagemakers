/*
 *  Maak een vraag-antwoord-''regel'' aan d.m.v. JLabel's
 * 
 *  Vraag x : ......
 *  Antwoord x : .......
 *  
 */

package be.janwagemakers.projector.view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Regel {
	private JLabel lblVraag;
	private JLabel lblAntw;

	public Regel(JFrame frame, int x, int y, float size) {
		// Bereken posities aan de hand van vergrotingsfactor size
		int x1 = (int) (x*size);
		int y1 = (int) (y*size);
		int x2 = (int) (630*size);
		int y2 = (int) (40*size);
				
		// Maak label Vraag x : ................
		lblVraag = new JLabel();
		lblVraag.setBounds(x1, y1, x2, y2);
		lblVraag.setFont(new Font("Tahoma", Font.PLAIN, (int) (24*size)));
		frame.getContentPane().add(lblVraag);
		
		// Schuif op naar beneden
		y1 = (int) (y1 + 40 * size);
		
		// Maak label Antwoord x : ................
		lblAntw = new JLabel();
		lblAntw.setBounds(x1, y1, x2, y2);
		lblAntw.setForeground(Color.blue);
		lblAntw.setFont(new Font("Tahoma", Font.PLAIN, (int) (24*size)));
		frame.getContentPane().add(lblAntw);
	}
	
	// Setter om String van label vraag aan te passen
	public void setVraag(String vraag) {
		lblVraag.setText("Vraag : " + vraag);
	}

	// Setter om String van label antwoord aan te passen
	public void setAntwoord(String antwoord) {
		lblAntw.setText("Antwoord : " + antwoord);
	}
}
