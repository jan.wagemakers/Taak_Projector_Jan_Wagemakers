/*
 * Model : Alle antwoorden/vragen worden hier bijgehouden :
 * 	- vragen in een String[] vragen
 * 	- antwoorden in een String[] antwoorden
 */

package be.janwagemakers.projector.model;
import java.util.Observable;

public class Model extends Observable {
	private String[] vragen = new String[4];
	private String[] antwoorden = new String[4];

	public Model() {
				
	}
	
	public String[] getVragen() {
		return vragen;
	}

	public String[] getAntwoorden() {
		return antwoorden;
	}

	// Aanpassen van vragen --> Observers op de hoogte brengen
	public void setVragen(String[] vragen) {
		this.vragen = vragen;
		this.setChanged();
		this.notifyObservers();
	}

	// Aanpassen van antwoorden --> Observers op de hoogte brengen
	public void setAntwoorden(String[] antwoorden) {
		this.antwoorden = antwoorden;
		this.setChanged();
		this.notifyObservers();
	}
	
	// Antwoorden leeg maken --> Observers op de hoogte brengen
	public void emptyAntwoorden() {
		antwoorden[0] = "???";
		antwoorden[1] = "???";
		antwoorden[2] = "???";
		antwoorden[3] = "???";
		this.setChanged();
		this.notifyObservers();
	}
}
