/*
 * Hier komen we terecht als er op de knop Update Vragen is geklikt
 * 
 */
package be.janwagemakers.projector.controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import be.janwagemakers.projector.model.Model;
import be.janwagemakers.projector.view.Beheer;

public class UpdateVragen implements ActionListener {
	private Model model;
	private Beheer beheer;

	public UpdateVragen(Model model, Beheer beheer) {
		this.model = model;
		this.beheer = beheer;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// model : inhoud van vragen vullen met hetgene we in beheer hebben ingegeven.
		model.setVragen(beheer.getVragen());
		// model : de antwoorden leegmaken, want als er nieuwe vragen getoond worden moeten de oude antwoorden verdwijnen
		model.emptyAntwoorden();
	}

}

