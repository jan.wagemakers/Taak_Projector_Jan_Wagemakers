/*
 * Hier komen we terecht als er op de knop update antwoorden is geklikt.
 */
package be.janwagemakers.projector.controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import be.janwagemakers.projector.model.Model;
import be.janwagemakers.projector.view.Beheer;

public class UpdateAntwoorden implements ActionListener {
	private Model model;
	private Beheer beheer;

	public UpdateAntwoorden(Model model, Beheer beheer) {
		this.model = model;
		this.beheer = beheer;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// model : inhoud van antwoorden vullen met hetgene we in beheer hebben ingegeven.
		model.setAntwoorden(beheer.getAntwoorden());
	}

}
